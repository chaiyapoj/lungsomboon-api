<?php

    namespace App\Http\Controllers;

    use Symfony\Component\HttpKernel\Exception\HttpException;
    use App\Content;

    class ContentController extends Controller
    {
        public function index()
        {
            $contents = Content::all();
            if (!$contents) {
                throw new HttpException(400, "Invalid data");
            }
            return response()->json(
                $contents,
                200
            );
        }

        public function show($id)
        {
            if (!$id) {
                throw new HttpException(400, "Invalid id");
            }
            $contents = Content::find($id);
            return response()->json([
                $contents,
            ], 200);
        }
    }
