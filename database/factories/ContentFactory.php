<?php

    use Faker\Generator as Faker;
    use App\Content;

    $factory->define(Content::class, function (Faker $faker) {
        return [
            'title' => $faker->name,
            'thumbnail' => 'http://google.co.th/img.png',
            'body' => str_random(20),
        ];
    });
