<?php

    use Illuminate\Http\Request;

    /*
    |--------------------------------------------------------------------------
    | API Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register API routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | is assigned the "api" middleware group. Enjoy building your API!
    |
    */

    /*Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });*/

/*    Route::group(array('prefix' => 'api/v1', 'middleware' => []), function () {
        Route::get('/private', function (Request $request) {
            return response()->json(["message" => "Hello from a private endpoint! You need to have a valid access token to see this."]);
        })->middleware('auth:api');
    });*/

    Route::get('/content', 'ContentController@index');
    Route::get('/content/{id}', 'ContentController@show');

